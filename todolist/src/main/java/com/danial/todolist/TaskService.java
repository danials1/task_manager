package com.danial.todolist;

import com.danial.todolist.model.Task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TaskService {
    @Autowired
    private final TaskRepository taskRepository;

    @Autowired
    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void addTask(Task task) {
        taskRepository.save(task);
    }

    public List<Task> getAllTasks() {
        List<Task> allTasks = new ArrayList<>();
        try {
            taskRepository.findAll().forEach(allTasks::add);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return allTasks;
    }

    public Task updateTask(Task newtask) {
        try {
            return taskRepository.save(newtask);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return newtask;
        }
    }


    public Optional<Task> getTaskById(Long id) {
        return taskRepository.findById(id);
    }

    public String updateTaskName(Long id) {
        try {
            taskRepository.deleteById(id);
            return "successful";
        } catch (Exception e) {
            return "no task with that id";
        }
    }

    public String updateTaskDescription(Long id) {
        try {
            taskRepository.deleteById(id);
            return "successful";
        } catch (Exception e) {
            return "no task with that id";
        }
    }

    public String updateTaskDueDate(Long id) {
        try {
            taskRepository.deleteById(id);
            return "successful";
        } catch (Exception e) {
            return "no task with that id";
        }
    }

    public String updateTaskPriority(Long id) {
        try {
            taskRepository.deleteById(id);
            return "successful";
        } catch (Exception e) {
            return "no task with that id";
        }
    }

    public String updateTaskStatus(Long id) {
        try {
            taskRepository.deleteById(id);
            return "successful";
        } catch (Exception e) {
            return "no task with that id";
        }
    }

    public String deleteTaskById(Long id) {
        try {
            taskRepository.deleteById(id);
            return "successful";
        } catch (Exception e) {
            return "no task with that id";
        }
    }
}