package com.danial.todolist.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.UUID;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "Task")
@Table(name = "task")
public class Task {
    @Id
    @SequenceGenerator(
            name = "task_sequence",
            sequenceName = "task_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "task_sequence"
    )
    @Column(
            name = "id",
            updatable = false
    )
    private Long id;


    @Column(
            name = "task_name",
            nullable = false
    )
    private String name;

    @Column(
            name = "description",
            nullable = true
    )
    private String description;

    @Column(
            name = "due_date",
            nullable = true
    )
    private String dueDate;

    @Column(
            name = "priority",
            nullable = false
    )
    private Integer Priority;
    @Column(
            name = "status",
            nullable = false
    )
    private Integer Status;

    public Task(@JsonProperty("id") Long id,
                @JsonProperty("name") String name,
                @JsonProperty("description") String description,
                @JsonProperty("dueDate") String dueDate,
                @JsonProperty("priority") Integer priority,
                @JsonProperty("status") Integer status) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.dueDate = dueDate;
        this.Priority = priority;
        this.Status = status;

    }

    private Task(){}

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Integer getStatus() {
        return Status;
    }

    public Integer getPriority() {
        return Priority;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public void setPriority(Integer priority) {
        Priority = priority;
    }

    public void setStatus(Integer status) {
        Status = status;
    }
}
