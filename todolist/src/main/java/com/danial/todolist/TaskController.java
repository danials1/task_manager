package com.danial.todolist;

import com.danial.todolist.model.Task;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RequestMapping("api/v1.1/todolist")
@RestController
public class TaskController {
    @Autowired
    private final TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @RequestMapping(value = "/addtask", method = RequestMethod.POST)
    public void addTask(@Validated @NonNull @RequestBody Task task) {
        taskService.addTask(task);
    }

    @RequestMapping(value = "/updatetask", method = RequestMethod.PUT)
    public void updateTask(@NonNull @RequestBody Task task) {
        taskService.updateTask(task);
    }

    @RequestMapping(value = "/getalltasks", method = RequestMethod.GET)
    @ResponseBody
    public List<Task> getAllTasks() {
        try {
            return taskService.getAllTasks();
        } catch (Exception ex) {
            throw ex;
        }
    }

    @RequestMapping(value = "/gettaskbyid/{id}", method = RequestMethod.GET)
    public Optional<Task> getTaskById(@PathVariable Long id) {
        try {
            return taskService.getTaskById(id);
        } catch (Exception ex) {
            throw ex;
        }
    }

    @RequestMapping(value = "/deletetaskbyid/{id}", method = RequestMethod.DELETE)
    public String deleteTaskById(@PathVariable Long id) {
        try {
            return taskService.deleteTaskById(id);
        } catch (Exception ex) {
            throw ex;
        }
    }

}
