package com.danial.todolist;

import com.danial.todolist.model.Task;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends CrudRepository<Task, Long> {
    @Query("SELECT t FROM Task t WHERE t.name = ?1")
    List<Task> findTaskByName(String name);

    //TODO: write a better function for searching the task description
    @Query("SELECT t FROM Task t WHERE t.description = ?1")
    Optional<Task> findTaskByDescription(String email);

    Optional<Task> findById(Long id);
    Optional<Task> findByName(String name);
    Optional<Task> findByDescription(String description);

}
